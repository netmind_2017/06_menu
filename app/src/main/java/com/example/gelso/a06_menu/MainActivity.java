package com.example.gelso.a06_menu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("Menu");

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.items,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id=item.getItemId();

        switch(id)
        {
            case R.id.MENU_1:
                Toast.makeText(getApplicationContext(), "Menu 1 ", Toast.LENGTH_SHORT).show();
                break;
            case R.id.MENU_2:
                Toast.makeText(getApplicationContext(), "Menu 2", Toast.LENGTH_SHORT).show();
                break;
            case R.id.MENU_3:
                Toast.makeText(getApplicationContext(), "Menu 3", Toast.LENGTH_SHORT).show();
                break;
            case R.id.MENU_4:
                Toast.makeText(getApplicationContext(), "Menu 4", Toast.LENGTH_SHORT).show();
                break;
            case R.id.MENU_5:
                Toast.makeText(getApplicationContext(), "Menu 5", Toast.LENGTH_SHORT).show();
                break;

        }

        return false;
    }

}
